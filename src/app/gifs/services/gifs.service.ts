import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Datum, SearchGifsResponse} from "../interface/gifs.interface";

@Injectable({
  providedIn: 'root'
})
export class GifsService {

  private _historial: string[] = [];

  private ENDPOINT = "https://api.giphy.com/v1/gifs/"
  private apikey: string = "ObKWoOh2U0GlkVpG5gjdWjQFDnMHyFsm"

  public resultados: Datum[] = [];

  get historial(): string[] {
    return [...this._historial];
  }

  constructor(private http: HttpClient) {
    this._historial = JSON.parse(localStorage.getItem('historial') || '[]');
    this.resultados = JSON.parse(localStorage.getItem('resultados') || '[]');
  }

  buscarGifs(query: string) {

    query = query.trim().toLowerCase();

    if (query.trim().length === 0) {
      return;
    }

    if (!this._historial.includes(query)) {
      this._historial.unshift(query);

    } else {
      this._historial = this._historial.filter(item => item !== query);
      this._historial.unshift(query);
    }

    if (this._historial.length > 10) {
      this._historial.pop();
    }

    localStorage.setItem('historial', JSON.stringify(this._historial));

    // console.log(this._historial);

    const params = new HttpParams()
      .set('api_key', this.apikey)
      .set('q', query)
      .set('limit', '10')


    this.http.get<SearchGifsResponse>(`${this.ENDPOINT}search`, {params})
      .subscribe({
        next: (data) => {
          // console.log(data.data);
          this.resultados = data.data;
          localStorage.setItem('resultados', JSON.stringify(data.data));
        }
      })

  }

}
