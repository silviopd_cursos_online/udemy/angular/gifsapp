import {Component, OnInit} from '@angular/core';
import {GifsService} from "../../gifs/services/gifs.service";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  get historial(): string[] {
    return this.gifService.historial;
  }

  constructor(private gifService: GifsService) {
  }

  ngOnInit(): void {
  }

  buscar(buscar: string) {
    this.gifService.buscarGifs(buscar)
  }
}
